#ifndef BANANA_H
#define BANANA_H

#include "banana_config.h"

// All interface functions should have the same prefix 'banana_'
void banana_give_me();
void banana_netcdf(const char* filename);

#endif // BANANA_H
