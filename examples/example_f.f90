#include <banana_config.h>

program example_f
  use banana, only : banana_give_me, &
                   & banana_netcdf
  implicit none

  call banana_give_me()
#ifdef BANANA_NETCDF_ENABLED
  call banana_netcdf('banana_f.nc')
#else
  print *, "Avoid calling 'banana_netcdf' because NetCDF is disabled."
#endif
end program example_f
