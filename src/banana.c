#ifdef HAVE_CONFIG_H
# include "config.h"
#endif // HAVE_CONFIG_H

#include <stdlib.h>
#include <stdio.h>

#ifdef BANANA_NETCDF_ENABLED
# include <netcdf.h>
# define ERRCODE 2
# define ERR(e) {printf("Error: %s\n", nc_strerror(e)); exit(ERRCODE);}
#endif // BANANA_NETCDF_ENABLED

#include "banana.h"

void banana_give_me() {
  printf("Here is your banana!\n");
}

void banana_netcdf(const char* filename) {
#ifdef BANANA_NETCDF_ENABLED
  int ncid, retval;

  if ((retval = nc_create(filename, NC_CLOBBER, &ncid)))
      ERR(retval);

  if ((retval = nc_close(ncid)))
      ERR(retval);

  printf("File '%s' is successfully created!\n", filename);
#else
  fprintf(stderr, "NetCDF is disabled\n");
  exit(EXIT_FAILURE);
#endif // BANANA_NETCDF_ENABLED
}
