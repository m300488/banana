#include <stdio.h>

#include <banana.h>

int main() {
  banana_give_me();
#ifdef BANANA_NETCDF_ENABLED
  banana_netcdf("banana.nc");
#else
  printf("Avoid calling 'banana_netcdf' because NetCDF is disabled.\n");
#endif // BANANA_NETCDF_ENABLED
}
