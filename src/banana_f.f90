module banana
  use iso_c_binding, only: c_char, &
                         & c_null_char, &
                         & c_size_t, &
                         & c_ptr, &
                         & c_associated

  implicit none

  private
  public::banana_give_me
  public::banana_netcdf

  ! Procedures that are bound directly:
  interface
    subroutine banana_give_me() bind(c)
    end subroutine banana_give_me
  end interface

  ! Procedures that need to be wrapped:
  interface
    subroutine orig_banana_netcdf(str) &
    bind(c, name = "banana_netcdf")
      import c_char
      character(c_char), intent(in) :: str(*)
    end subroutine orig_banana_netcdf
  end interface

  ! Binding to functions from the standard C library:
  interface
    function aux_strlen(cptr) result(f_result) &
    bind(c, name = "strlen")
      import c_ptr, c_size_t
      type(c_ptr), value :: cptr
      integer(c_size_t) :: f_result
    end function aux_strlen

    subroutine aux_free(cptr) &
    bind(c, name = "free")
      import c_ptr
      type(c_ptr), value :: cptr
    end subroutine aux_free
  end interface

contains
  subroutine banana_netcdf(str)
    character(*), intent(in) :: str

    call orig_banana_netcdf(aux_str2CStr(str))
  end subroutine banana_netcdf

  function aux_cPtr2Str(cptr, freePtr) result(str)
    type(c_ptr), intent(in) :: cptr
    logical :: freePtr
    character(:), allocatable :: str
    character(c_char), pointer :: arr(:)
    integer :: i

    if (c_associated(cptr)) then
      call c_f_pointer(cptr, arr, [aux_strlen(cptr)])
      allocate(character(len=size(arr)) :: str)
      forall(i = 1:size(arr)) str(i:i) = arr(i)
      if (freePtr) call aux_free(cptr)
    else
      str = ''
    end if
  end function aux_cPtr2Str

  function aux_str2CStr(str) result (cstr)
    character(*), intent(in) :: str
    character(:, c_char), allocatable :: cstr

    cstr = str(1:len_trim(str))//c_null_char
  end function aux_str2CStr

  function aux_cStr2Str(cstr) result (str)
    character(*, c_char), intent(in) :: cstr
    character(:), allocatable :: str

    str = cstr(1:scan(cstr, c_null_char) - 1)
  end function aux_cStr2Str

end module banana
